#pragma once

#include <SFML/System/Time.hpp>
#include "ptr.h"

class Engine;
class IDrawable;

class IActor {
public:
	virtual ~IActor() {};
	virtual void update(sf::Time time, sf::Time delta) = 0;
};

class IActorDrawable : public virtual IActor {
	template<typename T, typename... Args>
	friend sptr<T> makeActor(Engine& engine, Args&&... args);
protected:
	virtual sptr<IDrawable> drawable() = 0;
};

template<typename T, typename... Args>
sptr<T> makeActor(Engine& engine, Args&&... args) {
	static_assert(std::is_base_of<IActor, T>::value, "MakeActor type parameter is not a subclass of IActor.");
	sptr<T> a = make<T>(std::forward(args)...);
	if (std::is_base_of<IActorDrawable, T>::value) {
		auto actor_drawable = static_cast<IActorDrawable*>(a.get());
		engine.addDrawable(actor_drawable->drawable());
	}
	engine.addActor(static_cast<sptr<IActor>>(a));
	return a;
}