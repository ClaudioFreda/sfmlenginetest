#include "paddle.h"
#include <iostream>
PaddleGraphics::PaddleGraphics(Paddle& p) : paddle(p), rect(sf::Vector2f(10.F,20.F)) 
{
	rect.setFillColor(sf::Color::White);
}

void PaddleGraphics::drawOn(IDrawer & drawer)
{
	sf::Vector2f pivot = rect.getSize() / 2.F;
	rect.setPosition(paddle.position + pivot);
	drawer.draw(rect);
}

Paddle::Paddle() : drw(make_s<PaddleGraphics>(*this)) {}

void Paddle::update(sf::Time time, sf::Time delta)
{
	position += sf::Vector2f(1.F, 1.F) * 10.F * delta.asSeconds();
}