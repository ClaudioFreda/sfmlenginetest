#include "engine.h"
#include "actor.h"
#include "draw.h"
#include <iostream>

Engine::Engine() {}

using namespace std;

void Engine::gameloop()
{
	sf::VideoMode mode = sf::VideoMode(800,600);
	window.create(mode, "FredsGame");
	
	sf::Time time;
	sf::Clock clk;
	while (window.isOpen()) {
		sf::Event evt;
		while (window.pollEvent(evt))
		{
			handle(evt);
		}
		sf::Time delta = clk.restart();
		time += delta;
		update(time, delta);		
		render();
	}
}

void Engine::handle(sf::Event evt) {
	if (evt.type == sf::Event::Closed) {
		window.close();
	}
}

void Engine::update(sf::Time time, sf::Time delta)
{
	for (auto a : actors) {
		a->update(time, delta);
	}
}

void Engine::render()
{
	window.clear(sf::Color::Black);

	if (drawables_isDirty) {
		stable_sort(drawables.begin(), drawables.end(), [&](auto a, auto b) {
			return a->priority() < b->priority();
		});
		drawables_isDirty = false;
	}

	WindowDrawer window_drawer(window);
	for (auto d : drawables) {
		d->drawOn(window_drawer);
	}
	
	window.display();
}