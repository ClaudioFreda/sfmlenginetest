#pragma once
#include "actor.h"
#include "draw.h"
#include <SFML\Graphics.hpp>

class Paddle;
class PaddleGraphics : public virtual IDrawable {
private:
	sf::RectangleShape rect;
	Paddle& paddle;
public:
	PaddleGraphics(Paddle&);
	virtual void drawOn(IDrawer& drawer);
};

class Paddle : public virtual IActorDrawable {
	friend class PaddleGraphics;
private:
	sf::Vector2f position;
	sptr<PaddleGraphics> drw;
protected:
	virtual sptr<IDrawable> drawable() { return static_cast<sptr<IDrawable>>(drw); }
public:
	Paddle();
	virtual void update(sf::Time time, sf::Time delta);
};