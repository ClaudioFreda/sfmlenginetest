#pragma once

#include <memory>

template<typename T>
using uptr = std::unique_ptr<T>;

template<typename T>
using sptr = std::shared_ptr<T>;

template<typename T>
using wptr = std::weak_ptr<T>;

template<typename T, typename... Args>
sptr<T> make_s(Args&&... args) {
	return std::make_shared<T>(std::forward<Args>(args)...);
}

template<typename T, typename... Args>
uptr<T> make(Args&&... args) {
	return std::make_unique<T>(std::forward<Args>(args)...);
}