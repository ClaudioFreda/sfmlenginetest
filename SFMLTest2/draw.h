#pragma once

#include "ptr.h"

namespace sf {
	class Sprite;
	class Shape;
	class Text;
	class VertexArray;
	class RenderWindow;
	class RenderTexture;
}

class IDrawer {
public:
	virtual ~IDrawer() {}
	virtual void draw(const sf::Sprite&) = 0;
	virtual void draw(const sf::Shape&) = 0;
	virtual void draw(const sf::Text&) = 0;
	virtual void draw(const sf::VertexArray&) = 0;
};

class IDrawable {
public:
	virtual ~IDrawable() {}
	virtual void drawOn(IDrawer&) = 0;
	virtual int priority() { return 0; }
};

class WindowDrawer : public virtual IDrawer {
public:
	sf::RenderWindow& window;
	WindowDrawer(sf::RenderWindow& win) : window(win) {}
	virtual void draw(const sf::Sprite& sprite);
	virtual void draw(const sf::Shape& shape);
	virtual void draw(const sf::Text& text);
	virtual void draw(const sf::VertexArray& varray);
};

class TextureDrawer : public virtual IDrawer {
public:
	sf::RenderTexture& texture;
	TextureDrawer(sf::RenderTexture& tex) : texture(tex) {}
	virtual void draw(const sf::Sprite& sprite);
	virtual void draw(const sf::Shape& shape);
	virtual void draw(const sf::Text& text);
	virtual void draw(const sf::VertexArray& varray);
};