#include "draw.h"
#include <SFML/Graphics.hpp>

void WindowDrawer::draw(const sf::Sprite & sprite)
{
	window.draw(sprite);
}

void WindowDrawer::draw(const sf::Shape & shape)
{
	window.draw(shape);
}

void WindowDrawer::draw(const sf::Text & text)
{
	window.draw(text);
}

void WindowDrawer::draw(const sf::VertexArray & varray)
{
	window.draw(varray);
}

void TextureDrawer::draw(const sf::Sprite & sprite)
{
	texture.draw(sprite);
}

void TextureDrawer::draw(const sf::Shape & shape)
{
	texture.draw(shape);
}

void TextureDrawer::draw(const sf::Text & text)
{
	texture.draw(text);
}

void TextureDrawer::draw(const sf::VertexArray & varray)
{
	texture.draw(varray);
}
