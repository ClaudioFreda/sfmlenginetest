#include "ptr.h"
#include "engine.h"
#include "draw.h"
#include "paddle.h"
int main(int argc, char* argv) {
	Engine engine;
	sptr<Paddle> paddle = makeActor<Paddle>(engine);
	engine.gameloop();
	getchar();
}