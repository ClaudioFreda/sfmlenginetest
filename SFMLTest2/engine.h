#pragma once
#include "ptr.h"
#include <SFML/Graphics.hpp>
#include <iostream>
class IActor;
class IDrawable;

class Engine {
private:
	sf::RenderWindow window;
	std::vector<sptr<IActor>> actors;
	std::vector<sptr<IDrawable>> drawables;
	bool drawables_isDirty;
public:
	Engine();
	void gameloop();

	template<typename A>
	void addActor(A&& a) {
		actors.push_back(std::forward<A>(a));
		std::cout << "Added actor to list of actors.\n";
	}

	template<typename D>
	void addDrawable(D&& d) {
		auto dptr = static_cast<sptr<IDrawable>>(std::forward<D>(d));
		drawables.push_back(dptr);
		drawables_isDirty = true;
		std::cout << "Added drawable to list of drawables.\n";
	}

private:
	void update(sf::Time time, sf::Time delta);
	void render();
	void handle(sf::Event);	
};